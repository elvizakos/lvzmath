# LVzMath #

lvzmath is a lightweight library for mathematical calculations in JavaScript.

Demo: https://elvizakos.gitlab.io/lvzmath/

Usage: https://gitlab.com/elvizakos/lvzmath/-/wikis/home
