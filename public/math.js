/**
 * Function for evaluating math expressions.
 * @param {String} str - The math to be evaluated.
 * @return {String} Returns the result of the math expression, as a string.
 */
var lvzmath = function ( expression, cvars, clearFunctions ) {

	var mexpres = expression.split(/;/); // Breake string to expressions.
	var res = []; // Array of the results to return.

	if ( typeof cfunc == 'undefined' || cfunc === true ) {
		lvzmath.customFunctionNames = [];
		lvzmath.customFunctions = {};
	}

	if ( lvzmath.useVariables || lvzmath.useCustomFunctions ) {
		var cexpr;

		var variables = {}; // Object to store the variables of the expressions.

		if ( typeof cvars == 'object' ) variables = cvars;

		// walk through the expressions.
		for(var i=0;i<mexpres.length;i++)if(mexpres[i].trim().length>0){

			var loops = mexpres[i].match(/^\s*for \s*([a-z])\s*(?:[∈<]|in)\s*((?:\[[^,]+,\s*[^\]]+\])|(?:[{][^,]+(?:,[^,}]+)*[}])|(?:[-0-9.]+[.][.][-0-9.]+(?:[.][.][-0-9.]+)?)|(?:[^:]+))\s*[:]\s*((?:\n|.)*)$/m);

			// if expression is a loop
			if ( loops !== null ) {

				cexpr = loops[3].split(/\n/);
				var stp = '';
				var tmpexpr = '';

				var range = lvzmath.range ( loops[2], variables);

				if ( range === false ) return false;

				if ( typeof range.b != 'undefined' ) {
					range.b = parseFloat(range.b);
					range.e = parseFloat(range.e);
					if ( range.s === false ) range.s = (range.e - range.b) / 100;

					if ( range.s > 0 && range.b > range.e)   return false;
					else if ( range.s < 0 && range.b < range.e)   return false;
					else if ( range.e == range.b || range.s == 0) return false;

					// Fire event
					lvzmath.fireEvent('loopbegin', {
						"expression" : expression,
						"error" : false,
						"loop-expression" : loops[0],
						"loop-variable" : loops[1],
						"range" : range,
						"body" : loops[3]
					});

					if ( range.b < range.e ) for ( var j = range.b; j <= range.e; j += range.s ) {
						stp = '';
						for ( var k=0; k < cexpr.length; k ++ ) {
							tmpexpr = cexpr[k].split(/=/);
							if ( tmpexpr.length == 2 ) {
								variables[loops[1]] = {
									expressions : [{original: j+''}],
									'new' : false,
									value : j
								};
								mres = lvzmath( tmpexpr[1], Object.assign({}, variables),false);
								// Object.assign({},{
								// 	names: lvzmath.customFunctionNames,
								// 	functions: lvzmath.customFunctions
								// })
								if ( typeof variables[tmpexpr[0].trim()] == 'undefined' )
									variables[tmpexpr[0].trim()] = {
										expressions : [{original: tmpexpr[1]+''}],
										'new' : false,
										value : mres.result
									};
								else {
									variables[tmpexpr[0].trim()].expressions.push({original: tmpexpr[1]+''});
									variables[tmpexpr[0].trim()].value = mres.result;
								}
								stp += '; ' + tmpexpr[0] + ' = ' + mres.result + ' ';
							}
						}
						stp = loops[1] + ' = ' + j + '; ' + stp.substr(2);

						// Fire event
						lvzmath.fireEvent('loopstep', {
							"expression" : expression,
							"error" : false,
							"loop-expression" : loops[0],
							"loop-variable" : loops[1],
							"range" : range,
							"body" : loops[3],
							"step-value" : j,
							"step" : stp
						});
						
						res.push(stp);

					}
					else if ( range.b > range.e ) for ( var j = range.b; j >= range.e; j += range.s ) {
						console.log ( 'step : ' + j ) 
						stp = '';
						for ( var k=0; k < cexpr.length; k ++ ) {
							tmpexpr = cexpr[k].split(/=/);
							if ( tmpexpr.length == 2 ) {
								variables[loops[1]] = {
									expressions : [{original: j+''}],
									'new' : false,
									value : j
								};
								mres = lvzmath( tmpexpr[1], Object.assign({}, variables) );
								if ( typeof variables[tmpexpr[0].trim()] == 'undefined' )
									variables[tmpexpr[0].trim()] = {
										expressions : [{original: tmpexpr[1]+''}],
										'new' : false,
										value : mres.result
									};
								else {
									variables[tmpexpr[0].trim()].expressions.push({original: tmpexpr[1]+''});
									variables[tmpexpr[0].trim()].value = mres.result;
								}
								stp += '; ' + tmpexpr[0] + ' = ' + mres.result + ' ';
							}
						}
						stp = loops[1] + ' = ' + j + '; ' + stp.substr(2);

						// Fire event
						lvzmath.fireEvent('loopstep', {
							"expression" : expression,
							"error" : false,
							"loop-expression" : loops[0],
							"loop-variable" : loops[1],
							"range" : range,
							"body" : loops[3],
							"step-value" : j,
							"step" : stp
						});
						
						res.push(stp);

					}

				}
				else {
					// Fire event
					lvzmath.fireEvent('loopbegin', {
						"expression" : expression,
						"error" : false,
						"loop-expression" : loops[0],
						"loop-variable" : loops[1],
						"range" : range,
						"body" : loops[3]
					});
					for ( var j = 0; j < range.length; j++ ) {
						stp = '';
						for ( var k=0; k < cexpr.length; k ++ ) {
							tmpexpr = cexpr[k].split(/=/);
							if ( tmpexpr.length == 2 ) {
								variables[loops[1]] = {
									expressions : [{original: j+''}],
									'new' : false,
									value : range[j]
								};
								mres = lvzmath( tmpexpr[1], Object.assign({}, variables) );
								if ( typeof variables[tmpexpr[0].trim()] == 'undefined' )
									variables[tmpexpr[0].trim()] = {
										expressions : [{original: tmpexpr[1]+''}],
										'new' : false,
										value : mres.result
									};
								else {
									variables[tmpexpr[0].trim()].expressions.push({original: tmpexpr[1]+''});
									variables[tmpexpr[0].trim()].value = mres.result;
								}
								stp += '; ' + tmpexpr[0] + ' = ' + mres.result + ' ';
							}
						}

						stp = loops[1] + ' = ' + range[j] + '; ' + stp.substr(2);

						// Fire event
						lvzmath.fireEvent('loopstep', {
							"error" : false,
							"expression" : expression,
							"loop-expression" : loops[0],
							"loop-variable" : loops[1],
							"range" : range,
							"body" : loops[3],
							"step-value" : range[j],
							"step" : stp
						});
						
						res.push(stp);
					}
				}

				// Fire event
				lvzmath.fireEvent('loopend', {
					"error" : false,
					"expression" : expression,
					"loop-expression" : loops[0],
					"loop-variable" : loops[1],
					"range" : range,
					"body" : loops[3]
				});

			}
			else {

				cexpr=mexpres[i].split(/=/gm);

				if ( cexpr.length == 1 ) {

					// Search and replace all constants with their values.
					if ( lvzmath.useConstants )
						for ( var j in lvzmath.constants)
							mexpres[i] = mexpres[i].replace(RegExp('\\b'+j+'\\b','gmu'), lvzmath.constants[j]);

					// Search and replace all variables with their values.
					for ( var j in variables) mexpres[i] = mexpres[i].replace(RegExp('\\b'+j+'\\b','gm'), variables[j].value);

					// Add the result of the expression to the return array.
					res.push(lvzmath.functions(mexpres[i]));
				}
				else if ( cexpr.length > 2 ) {

					// Fire event.
					lvzmath.fireEvent('multiple_assignment', {
						"error" : true,
						"full_expression" : expression,
						"expression" : mexpres[i],
						"message" : "can't assign to more than one variable per expression"
					});

					// Stop execution and return false.
					return false;

				}
				// Assign value to a variable or create a function
				else if ( cexpr.length == 2 ) {

					// Custom  function
					var afunc = cexpr[0].trim().match(/^([A-Za-z][A-Za-z0-9_]*)\s*[(]([^(]*)[)]$/m);

					if ( afunc !== null && lvzmath.useCustomFunctions === true ) {

						afunc[2] = afunc[2].split(/,/);
						for ( var j=afunc[2].length-1;j>=0;j--)
							if (afunc[2][j].trim() == '')
								afunc[2].splice(j, 1);

						lvzmath.customFunctionNames.push( afunc[1] );
						lvzmath.customFunctions[afunc[1]] = {
							name : afunc[1],
							args : afunc[2],
							body : cexpr[1],
							type : 1
						};

						// Fire event.
						lvzmath.fireEvent('create_function', {
							"error" : false,
							"full_expression" : expression,
							"expression" : mexpres[i],
							"function_name" : afunc[1],
							"function" : lvzmath.customFunctions[afunc[1]]
						});

					}
					else if ( lvzmath.useVariables ) {

						// Check the variable's name.
						var avar = cexpr[0].trim().match(/^[A-Za-zΑ-Ωα-ω][A-Za-zΑ-Ωα-ω0-9_]*$/u);

						if ( avar === null ) { // Invalid variable name.

							// Fire event.
							lvzmath.fireEvent('invalid_var_name', {
								"error" : true,
								"full_expression" : expression,
								"expression" : mexpres[i],
								"variable_name" : cexpr[0].trim(),
								"message" : "Not a valid variable name. Variable names must begin with a capital\n or small latin letter and can be followed by latin letters,\nnumbers and the \"_\" character."
							});

							// Stop execution and return false.
							return false;
						}
						avar = avar[0];
						if ( lvzmath.reservedNames.indexOf(avar) > -1 ) { // Variable use a reserved name.

							// Fire event.
							lvzmath.fireEvent('reserved_name', {
								"error" : true,
								"full_expression" : expression,
								"expression" : mexpres[i],
								"variable_name" : avar,
								"message" : "This is a reserved name and cannot be used. Look the property \"lvzmath.reservedNames\" for all reserved names"
							});

							// Stop execution and return false.
							return false;
						}

						// Variable is not set so create it.
						if ( typeof variables[avar] == 'undefined' ){
							variables[avar] = {"expressions" : [],"value":0,"new":true};
							lvzmath.fireEvent('create_variable', {
								"error" : false,
								"full_expression" : expression,
								"expression" : cexpr[1],
								"variable_name" : avar
							});
						}

						var tmpexpr = {
							"original":cexpr[1], // Save the original expression.
							"expression":"",
							"previous_value" : variables[avar].value,
							"value":""
						};

						// Search and replace all constants with their values.
						if ( lvzmath.useConstants )
							for ( var j in lvzmath.constants)
								cexpr[1] = cexpr[1].replace(RegExp('\\b'+j+'\\b','gmu'), lvzmath.constants[j]);

						// Search and replace all variables with their values.
						var chb='[ 	\\t\\n\\s.,+\\-/\\^\\\\()\\[\\]|:;\'"{}]';
						for ( var j in variables ) {
							cexpr[1] = cexpr[1].replace(RegExp('\\b'+j+'\\b','gmu'), variables[j].value);
							// console.log ( '('+chb+'|^)' + j + '($|'+chb+')' ) ;
							// console.log ( cexpr ) ;
							// cexpr[1] = cexpr[1].replace(RegExp('('+chb+'|^)' + j + '($|'+chb+')','gmu'),'$1'+ variables[j].value+'$2');
							// "sdf sdf 123 aΠa πΠ +Π+ 234".replace(/([\s.,+-/\^\\()\[\]]|^)Π($|[\s.,+-/\^\\()\[\]])/gmu, '$13.14$2')
							// "Π sdf sdf 123 aΠa πΠ +Π+ 234 Π".replace(/([ 	\t\n\s.,+\-/\^\\()\[\]|:;'"{}]|^)Π($|[ 	\t\n\s.,+\-/\^\\()\[\]|:;'"{}])/gmu, '$13.14$2')
						}

						tmpexpr.expression = cexpr[1]; // Save the expression.

						// Execute the expression, get the value and store it to the variable.
						variables[avar].value = tmpexpr.value = lvzmath.functions(cexpr[1]);

						variables[avar].expressions.push(tmpexpr);

						// Add the result to the return array.
						res.push(variables[avar].value);

						if ( variables[avar]['new'] === true ) {
							variables[avar]['new'] = false;
						}
						else {
							lvzmath.fireEvent('update_variable', {
								"error" : false,
								"full_expression" : expression,
								"expression" : cexpr[1],
								"variable_name" : avar,
								"variable_value" : variables[avar].value
							});
						}

						// Fire event
						lvzmath.fireEvent('assign_variable', {
							"error" : false,
							"full_expression" : expression,
							"expression" : cexpr[1],
							"variable_name" : avar,
							"variable_value" : variables[avar].value
						});
					}
				}
			}
		}
	}
	else {
		// walk through the expressions.
		for(var i=0;i<mexpres.length;i++)if (mexpres[i].trim().length > 0){
			// Execute the expression and store the result to the return array.
			res.push(lvzmath.functions(mexpres[i]));
		}
	}

	//console.log ( variables );
	//console.log ( res );

	// Fire event
	lvzmath.fireEvent('finish_execution', {
		"error" : false,
		"expression" : expression,
		"variables" : variables,
		"results" : res
	});

	// If result array has only one element return that element instead of the whole array.
	if ( res.length == 1 ) return {
		"variables" : variables,
		"result" : res[0]
	};

	// Return the results array. 
	return {
		"variables" : variables,
		"result" : res
	};

};

/**
 * Static method for evaluating functions in mathematical expressions.
 * @param {String} expression - The string of the mathematical expression.
 * @return {String} The result of the given expression.
 */
lvzmath.functions = function ( expression ) {

	// Calculate factorial
	var ret = '('+expression.replace(/([0-9]+)[!]/gm,function(p0, p1, offset, input_string){
		var n = parseInt(p1);
		if ( n === 0 ) return 1;
		var r = 1;
		for ( var i=2; i<=n; i++)r*=i;
		return r+'';
	})+')';

	// Calculate anti-factorial
	ret = '('+ret.replace(/[!]([0-9]+)/gm,function(p0, p1, offset, input_string){
		var n = parseInt(p1);
		if ( n === 0 ) return 1;
		var r = 1;
		for ( var i=2; i<=n; i++)r*=1/i;
		return r+'';
	})+')';

	// Build lists using "first..last..incr" format
	var n = '-?[0-9]+(?:[.][0-9]+)?';
	ret = ret.replace(RegExp('('+n+')[.][.]('+n+')(?:[.][.]('+n+'))?','m'),function(...m){
		var ret = '';
		var n1 = parseFloat(m[1]);
		var n2 = parseFloat(m[2]);
		var n3 = typeof m[3] !== 'undefined' ? parseFloat(m[3]) : 1;
		if ( n3 == 0 ) return '';

		if ( n1 < n2 ) {
			if ( n3 < 0 ) return '';
			for ( var i = n1; i <= n2; i+= n3) ret += ','+i;
			ret = ret.substr(1);

			// Fire event.
			lvzmath.fireEvent('create_list', {
				"error" : false,
				"full_expression" : expression,
				"begin" : n1,
				"end" : n2,
				"increment" : n3
			});

			return ret;
		}
		else if ( n1 > n2 ) {
			if ( n3 > 0 ) return '';
			for ( var i = n1; i >= n2; i+= n3) ret += ','+i;
			ret = ret.substr(1);

			// Fire event.
			lvzmath.fireEvent('create_list', {
				"error" : false,
				"full_expression" : expression,
				"begin" : n1,
				"end" : n2,
				"increment" : n3
			});

			return ret;
		}
	});

	var func = '';
	var p,args;
	var tmpmid,t,pres;

	// Custom functions
	if ( lvzmath.useCustomFunctions === true ) {
		var j = 0;
		p = {};
		while ( true ) {
			var bpos = ret.indexOf('{');
			if ( bpos === -1 ) break;
			p.left = ret.substr(0,bpos);
			p.middle = ret.substr(bpos);
			var epos = p.middle.indexOf('}');
			if ( epos === -1 ) break;
			p.right = p.middle.substr(epos+1);
			p.middle = p.middle.substr(1,epos-1);

			func = p.left.match(/(?<=[^a-zA-Z0-9])([a-z][a-z0-9]*)\s*$/);

			if ( func === null ) break;

			lvzmath.customFunctionNames.push( func[1] );
			lvzmath.customFunctions[func[1]] = {
				name : func[1],
				body : p.middle,
				type : 2
			};

			// Fire event.
			lvzmath.fireEvent('create_function', {
				"error" : false,
				"full_expression" : expression,
				"expression" : ret,
				"function_name" : func[1],
				"function" : lvzmath.customFunctions[func[1]]
			});

			ret = p.left.substr(0,p.left.length - func[0].length ) + p.right;

			if ( j > 10000) break;
			j++;
		}
	}
	// end custom functions

	while ( (p = lvzmath.getFirstMostInnerParenthesis(ret)) !== false ) {
		if ( lvzmath.useFunctions === true )
			func = p.left.trimRight().match(/(?<=[^a-zA-Z0-9])[a-z][a-z0-9]*$/);
		else
			func = null;

		// Just parenthesis
		if (func === null ) {

			pres = lvzmath.evaluate(p.middle);
			ret=p.left + pres + p.right;

			// Fire event.
			lvzmath.fireEvent('parenthesis', {
				"error" : false,
				"full_expression" : expression,
				"before" : p.left.substr(1),
				"left_part" : p.left.substr(1),
				"after"  : p.right.substr(0,p.right.length-1),
				"right_part" : p.right.substr(0,p.right.length-1),
				"expression" : p.middle,
				"mid_part"   : p.middle,
				"result"     : pres,
				"result"      : ret
			});

			// Fire event.
			lvzmath.fireEvent('step', {
				"error" : false,
				"step_type" : "parenthesis",
				"full_expression" : expression,
				"expression" : ret,
				"left_part" : p.left.substr(1),
				"right_part" : p.right.substr(0,p.right.length-1),
				"mid_part" : p.middle,
				"result" : ret
			});

		}

		// Custom functions not in Math object with undefined number of arguments
		else if ( ['sum','avg','median','mode',
				   'range','count','left','right','mid',
				   'replace','indexof','list','item',
				   'root','seq','fibonacci','fseq',
				   'reverse','sort','first','last',
				   'lroll','rroll','greatestcommondivisor',
				   'gcd','leastcommonmultiple','lcm'].indexOf(func[0]) > -1 ) {

			args = p.middle.split(/,/);
			args = args.map(x => parseFloat(lvzmath.evaluate(x)));

			// Fire event.
			lvzmath.fireEvent('run_function', {
				"error" : false,
				"full_expression" : expression,
				"arguments" : args,
				"function_name" : func[0]
			});

			tmpmid = 0;
			if ( func[0] == 'sum' ) {
				for ( var i=0; i<args.length; i++ )tmpmid+=args[i];
			}
			else if ( func[0] == 'avg' ) {
				for ( var i=0; i<args.length; i++ )tmpmid+=args[i];
				tmpmid = tmpmid / args.length;
			}
			else if ( func[0] == 'root' ) {
				args[0] = parseFloat(lvzmath.evaluate(args[0]));
				args[1] = parseFloat(lvzmath.evaluate(args[1]));

				tmpmid = Math.pow(args[0],1/args[1]);

			}
			else if ( func[0] == 'median' ) {
				tmpmid=[];
				for ( var i=0; i<args.length; i++ )tmpmid.push(parseFloat(args[i]));
				tmpmid.sort(function (a,b){return a-b;});
				if ( tmpmid.length & 1 === 1 ) {
					tmpmid = tmpmid[ Math.floor( tmpmid.length / 2 ) ]
				}
				else {
					tmpmid = ( tmpmid[ Math.ceil( tmpmid.length / 2) ] + tmpmid[ Math.floor(tmpmid.length / 2)-1 ] ) / 2;
				}
			}
			else if ( func[0] == 'mode' ) {
				tmpmid={};
				var m=0, mn;
				for ( var i=0; i<args.length; i++ ){
					t = parseFloat(args[i]);
					if ( typeof tmpmid[t] == 'undefined' ) tmpmid[t] = 1;
					else tmpmid[t]++;
					if ( tmpmid[t] > m ) {
						m = tmpmid[t];
						mn = t;
					}
				}

				if ( m > 1 )  tmpmid = mn
				else tmpmid = '';

			}
			else if ( func[0] == 'range' ) {
				tmpmid=[];
				for ( var i=0; i<args.length; i++ )tmpmid.push(parseFloat(args[i]));
				tmpmid.sort(function (a,b){return a-b;});
				tmpmid = tmpmid[tmpmid.length - 1] - tmpmid[0];
			}
			else if ( func[0] == 'count' ) {
				if ( args[0] === NaN ) tmpmid = 0;
				else tmpmid = args.length;
			}
			else if ( func[0] == 'left' ) {
				var l = args.shift();
				tmpmid=args.slice(0, parseInt(l)).join(',');
			}
			else if ( func[0] == 'right' ) {
				var r = args.shift();
				tmpmid=args.slice(-parseInt(r)).join(',');
			}
			else if ( func[0] == 'mid' ) {
				var ms = parseInt(args.shift());
				var ml = parseInt(args.shift());
				tmpmid = args.slice(ms, ms+ml).join(',');
			}
			else if ( func[0] == 'replace' ) {
				var m = args.shift();
				var r = args.shift();
				tmpmid = args.map(item => item == m ? r : item).join(',');
			}
			else if ( func[0] == 'indexof' ) {
				var m = args.shift();
				tmpmid = args.indexOf(m);
			}
			else if ( func[0] == 'seq' ) {
				tmpmid = '';
				// seq [OPTION]... LAST
				// seq [OPTION]... FIRST LAST
				// seq [OPTION]... FIRST INCREMENT LAST

				switch ( args.length ) {
				case 1:
					var l = parseInt(args[0]);
					tmpmid = Array.from({length: l}, (_, i) => i + 1).join(',');
					break;
				case 2:
					var s = parseInt(args.shift());
					var l = parseInt(args[0]) - s + 1;
					tmpmid = Array.from({length: l}, (_, i) => i + s).join(',');
					break;
				case 3:
					var s = parseInt(args.shift());
					var stp = parseInt(args.shift());
					var l = parseInt(args.shift());
					for ( var k = s; k <= l; k += stp ) tmpmid +=','+k;
					tmpmid=tmpmid.substr(1);
					break;
				}
			}
			else if ( func[0] == 'list' ) {
				tmpmid = '';
				switch ( args.length ) {
				case 1:
					var l = parseInt(args[0]);
					tmpmid = Array.from({length: l}, (_, i) => i + 1).join(',');
					break;

				case 2:
					var s = parseInt(args.shift());
					var l = parseInt(args.shift());
					tmpmid = Array.from({length: l}, (_, i) => i + s).join(',');
					break;

				case 3:
					var s = parseInt(args.shift());
					var l = parseInt(args.shift());
					var stp = parseInt(args.shift());
					tmpmid = Array.from({length: l}, (_, i) => ( i * stp ) + s).join(',');
					break;
				}
			}
			else if ( func[0] == 'item' ) {
				tmpmid = '';
				var i = parseInt(args.shift());
				if ( i >= 0 && i < args.length ) tmpmid = args[i];
			}
			else if ( func[0] == 'first' ) {
				tmpmid = args[0];
			}
			else if ( func[0] == 'last' ) {
				tmpmid = args[args.length - 1];
			}
			else if ( func[0] == 'fibonacci' ) {
				args[0] = parseInt(args[0]);
				if ( args[0] == 0 )
					tmpmid = '0';
				else if ( args[0] == 1 )
					tmpmid = '1';
				else if ( args[0] > 1 ) {
					tmpmid = 1;
					var ptmpmid = 0, pptmpmid = 0;
					for ( var i = 1; i < args[0]; i ++ ) {
						pptmpmid = tmpmid;
						tmpmid += ptmpmid;
						ptmpmid = pptmpmid;
					}
				}
				else tmpmid = '';
			}
			else if ( func[0] == 'lroll' ) {
				i = args.pop();
				args.unshift(i);
				tmpmid = args.join(',');
			}
			else if ( func[0] == 'rroll' ) {
				i = args.shift();
				args.push(i);
				tmpmid = args.join(',');
			}
			else if ( func[0] == 'fseq' ) {
				args[0] = parseInt(args[0]);
				if ( args[0] == 0 )
					tmpmid = '0';
				else if ( args[0] == 1 )
					tmpmid = '0,1';
				else if ( args[0] > 1 ) {
					tmpmid = '0,1';
					var pp = 1, ppp = 0, pppp = 0;
					for ( var i = 1; i < args[0]; i ++ ) {
						pppp = pp;pp += ppp;ppp = pppp;
						tmpmid+=','+pp;
					}
				}
				else tmpmid = '';
			}
			else if ( func[0] == 'reverse' ) {
				tmpmid = args.reverse().join(',');
			}
			else if ( func[0] == 'sort' ) {
				tmpmid = args.sort().join(',');
			}
			else if ( func[0] == 'greatestcommondivisor' || func[0] == 'gcd' ) {
				tmpmid=""
				var pd, d;
				pd = true;
				for ( var j = 0; j < args.length; j ++ ) {
					args[j] = Math.floor(parseInt(args[j]));
					if ( args[j] <= 0 ) { pd = false; tmpmid=" gcd: Error "; break; }
				}
				if ( pd == true ) {
					var mx = Math.floor(Math.max(...args) / 2);
					for ( var i = mx; i > 0; i-- ) {
						pd = true;
						for ( var j = 0; j < args.length; j++ ) {
							d = args[j] / i;
							if ( d > Math.floor(d)) {
								pd = false;
								break;
							}
						}
						if ( pd === true ) break;
					}
					tmpmid=i;
				}
			}
			else if ( func[0] == 'leastcommonmultiple' || func[0] == 'lcm' ) {
				tmpmid=""
				var pd, m,d;
				pd = true;
				for ( var j = 0; j < args.length; j ++ ) {
					args[j] = Math.floor(parseInt(args[j]));
					if ( args[j] <= 0 ) { pd = false; tmpmid=" lcm: Error "; break; }
				}
				args.sort().reverse();
				m = args[0] - 1;
				while ( pd == true ) {
					pd = false;
					m++;
					for ( var i=0; i<args.length; i++) {
						d = m / args[i];
						if ( d > Math.floor(d) ) { pd = true; break; }
					}
				}
				tmpmid=m;
			}

			// Fire event.
			lvzmath.fireEvent('step', {
				"error" : false,
				"step_type" : "run function",
				"full_expression" : expression,
				"expression" : ret,
				"action" : func[0],
				"arguments" : args,
				"left_part" : p.left.substr(1),
				"right_part" : p.right.substr(0,p.right.length-1),
				"mid_part" : p.middle,
				"result" : tmpmid
			});

			p.left=p.left.trim();
			ret=p.left.substr(0,p.left.length-func[0].length) + tmpmid + p.right;
		}

		// Functions with no argument
		else if ( ['random'].indexOf(func[0]) > -1 ) {
			// Fire event.
			lvzmath.fireEvent('run_function', {
				"error" : false,
				"full_expression" : expression,
				"arguments" : [],
				"function_name" : func[0]
			});

			tmpmid = Math[func[0]]();

			// Fire event.
			lvzmath.fireEvent('step', {
				"error" : false,
				"step_type" : "run function",
				"full_expression" : expression,
				"expression" : ret,
				"action" : func[0],
				"arguments" : args,
				"left_part" : p.left.substr(1),
				"right_part" : p.right.substr(0,p.right.length-1),
				"mid_part" : p.middle,
				"result" : tmpmid
			});

			p.left=p.left.trim();
			ret=p.left.substr(0,p.left.length-func[0].length) + Math[func[0]]() + p.right;
		}

		else if ( func[0] == 'factors' ) {
			tmpmid = p.middle;
			p.middle = lvzmath.evaluate(p.middle);

			var on =  parseInt(p.middle);
			var n = parseInt(on / 2);
			var r = [1];

			var t;
			while ( n > 1 ) {
				t = on / n;
				if ( t === parseInt(t) ) r.push(t);
				n--;
			}

			r = r.join(',');
			lvzmath.fireEvent('step', {
				"error" : false,
				"step_type" : "run function",
				"expression" : ret,
				"action" : func[0],
				"arguments" : [tmpmid],
				"left_part" : p.left.substr(1),
				"right_part" : p.right.substr(0,p.right.length-1),
				"result" : r
			});

			p.left=p.left.trim();
			ret=p.left.substr(0,p.left.length-func[0].length) + r + p.right;
		}

		// Round function
		else if ( func[0] == 'round' ) {
			args = p.middle.split(/,/);

			if (args.length == 2 ) {
				args[0] = parseFloat(lvzmath.evaluate(args[0]));
				args[1] = parseInt(lvzmath.evaluate(args[1]));

				// Run event.
				lvzmath.fireEvent('run_function', {
					"error" : false,
					"full_expression" : expression,
					"arguments" : args,
					"function_name" : func[0]
				});

				args = (args[0].toFixed(args[1])+'');
			}
			else {
				args = lvzmath.evaluate(p.middle);

				// Run event.
				lvzmath.fireEvent('run_function', {
					"error" : false,
					"full_expression" : expression,
					"arguments" : [args],
					"function_name" : func[0]
				});

				args = Math.round(args);

			}

			// Fire event.
			lvzmath.fireEvent('step', {
				"error" : false,
				"step_type" : "run function",
				"full_expression" : expression,
				"expression" : ret,
				"action" : func[0],
				"arguments" : args,
				"left_part" : p.left.substr(1),
				"right_part" : p.right.substr(0,p.right.length-1),
				"mid_part" : p.middle,
				"result" : args
			});

			p.left=p.left.trim();
			ret=p.left.substr(0,p.left.length-func[0].length) + args + p.right;

		}

		// Functions of the Math collection with one argument
		else if ( ['abs','acos','acosh','asin','asinh','atan','atanh',
				   'ceil','cbrt','cos','cosh','exp','expm1','floor',
				   'log','log10','log1p','log2','sign','sin',
				   'sinh','sqrt','tan','tanh','trunc'
				  ].indexOf(func[0]) > -1 ) {

			// Run event.
			lvzmath.fireEvent('run_function', {
				"error" : false,
				"full_expression" : expression,
				"arguments" : [p.middle],
				"function_name" : func[0]
			});

			tmpmid = p.middle;
			p.middle = lvzmath.evaluate(p.middle);

			// Fire event.
			lvzmath.fireEvent('step', {
				"error" : false,
				"step_type" : "run function",
				"full_expression" : expression,
				"expression" : ret,
				"action" : func[0],
				"arguments" : args,
				"left_part" : p.left.substr(1),
				"right_part" : p.right.substr(0,p.right.length-1),
				"mid_part" : tmpmid,
				"result" : p.middle
			});

			p.left=p.left.trim();
			ret=p.left.substr(0,p.left.length-func[0].length) + Math[func](parseFloat(p.middle)) + p.right;
		}

		// Functions of the Math collection, with two arguments
		else if ( ['pow','atan2'].indexOf(func[0]) > -1 ) {
			args = p.middle.split(/,/);

			args[0] = lvzmath.evaluate(args[0]);
			args[1] = lvzmath.evaluate(args[1]);

			// Run event.
			lvzmath.fireEvent('run_function', {
				"error" : false,
				"full_expression" : expression,
				"arguments" : args,
				"function_name" : func[0]
			});

			tmpmid = Math[func[0]](parseFloat(args[0]),parseFloat(args[1]));

			// Fire event.
			lvzmath.fireEvent('step', {
				"error" : false,
				"step_type" : "run function",
				"full_expression" : expression,
				"expression" : ret,
				"action" : func[0],
				"arguments" : args,
				"left_part" : p.left.substr(1),
				"right_part" : p.right.substr(0,p.right.length-1),
				"mid_part" : p.middle,
				"result" : tmpmid
			});
			p.left=p.left.trim();
			ret=p.left.substr(0,p.left.length-func[0].length) + tmpmid + p.right;
		}

		// Functions of the Math collection, with undefined number of arguments
		else if ( ['max','min'].indexOf(func[0]) > -1 ) {
			args = p.middle.split(/,/);
			args = args.map(x => parseFloat(lvzmath.evaluate(x)));

			// Run event.
			lvzmath.fireEvent('run_function', {
				"error" : false,
				"full_expression" : expression,
				"arguments" : args,
				"function_name" : func[0]
			});

			tmpmid = Math[func](...args);

			// Fire event.
			lvzmath.fireEvent('step', {
				"error" : false,
				"step_type" : "run function",
				"full_expression" : expression,
				"expression" : ret,
				"action" : func[0],
				"arguments" : args,
				"left_part" : p.left.substr(1),
				"right_part" : p.right.substr(0,p.right.length-1),
				"mid_part" : p.middle,
				"result" : tmpmid
			});
			p.left=p.left.trim();
			ret=p.left.substr(0,p.left.length-func[0].length) + tmpmid + p.right;
		}

		// Custom functions
		else if ( lvzmath.customFunctionNames.indexOf ( func[0]) > -1 ) {
			args = p.middle.split(/,/);
			args = args.map(x => parseFloat(lvzmath.evaluate(x)));
			tmpmid = lvzmath.customFunctions[func[0]].body;

			if ( lvzmath.customFunctions[func[0]].type == 1 ) {
				for ( var k = 0; k < lvzmath.customFunctions[func[0]].args.length; k ++ ) {
					if ( typeof args[k] != 'undefined' && lvzmath.customFunctions[func[0]].args[k].trim() !== "" )
						tmpmid = tmpmid.replace(RegExp(lvzmath.customFunctions[func[0]].args[k],'gm'), args[k] );
				}
			}
			else if ( lvzmath.customFunctions[func[0]].type == 2 ) {
				tmpmid = tmpmid.replace (/\bx([0-9n]*)/gm,function(p0,p1,o,is) {
					if ( p1 == 'n' ) {
						return args.join(',');
					}
					else {
						// p1 = p1-1;
						if ( typeof args[p1] != 'undefined' ) return args[p1];
					}
					if ( args.length > 0 ) return args[0];
					return '';
				});
			}

			// Fire event.
			lvzmath.fireEvent('run_custom_function', {
				"error" : false,
				"full_expression" : expression,
				"arguments" : args,
				"function_name" : func[0],
				"function" : lvzmath.customFunctions[func[0]]
			});

			// Fire event.
			lvzmath.fireEvent('run_function', {
				"error" : false,
				"full_expression" : expression,
				"arguments" : args,
				"function_name" : func[0]
			});

			tmpmid = lvzmath.evaluate(tmpmid);

			// Fire event.
			lvzmath.fireEvent('step', {
				"error" : false,
				"step_type" : "run function",
				"full_expression" : expression,
				"expression" : ret,
				"action" : func[0],
				"arguments" : args,
				"left_part" : p.left.substr(1),
				"right_part" : p.right.substr(0,p.right.length-1),
				"mid_part" : p.middle,
				"result" : tmpmid
			});

			p.left=p.left.trim();
			ret=p.left.substr(0,p.left.length-func[0].length) + tmpmid + p.right;
		}

		// Undefined functions
		else {

			if ( func[0] ) {
				// Run event.
				lvzmath.fireEvent('function_undefined', {
					"error" : true,
					"full_expression" : expression,
					"arguments" : p.middle.split(/,/),
					"function_name" : func[0]
				});
				break;
			}
			else {
				ret=p.left + lvzmath.evaluate(p.middle) + p.right;
			}
		}

	}

	return ret;
};

/**
 * Function for evaluating simple math expressions such 
 * as powers, roots, multiplications, divisions, additions and subtractions.
 *
 * @param {String} str - The math expression to evaluate.
 * @return {String} The result of the expression.
 */
lvzmath.evaluate = function ( str ) {
	var ret = str;
	var cpos;
	var r;
	var n1 = '(?<number1>-?[0-9]+(?:[.][0-9]+)?)'; // Regular expression for capturing the first number.
	var n2 = '(?<number2>-*[0-9]+(?:[.][0-9]+)?)'; // Regular expression for capturing the second number.
	var cn1, cn2; // Variables for storing and format the captured numbers.

	// Powers and roots
	do {
		// Get the first expression
		cpos = RegExp(n1 +'\\s*(\\^|[*]{2}|[\/]{2}|√)\\s*'+n2).exec(ret);

		r = '';
		if ( cpos !== null ) { // if expression is found

			// get first number
			cn1 = cpos.groups.number1.trim().replace(/^(-{2})+/,'');

			// get second number
			cn2 = cpos.groups.number2.trim().replace(/^(-{2})+/,'');

			if ( cpos[2] == '**' || cpos[2] == '^' ) {
				// expression is power
				r = Math.pow(parseFloat(cn1), parseFloat(cn2))+'';
			}
			else if ( cpos[2] == '//' ) {
				// expression is root
				r = Math.pow(parseFloat(cn1), 1 / parseFloat(cn2))+'';
			}
			else if ( cpos[2] == '√' ) {
				// expression is root
				r = Math.pow(parseFloat(cn2), 1 / parseFloat(cn1))+'';
			}

			// Fire event.
			lvzmath.fireEvent('step', {
				"error" : false,
				"step_type" : "powers and roots",
				"full_expression" : str,
				"expression" : ret,
				"action" : cpos[2],
				"number1" : cpos.groups.number1,
				"number2" : cpos.groups.number2,
				"left_part" : ret.substr(0, cpos.index),
				"right_part" : ret.substr(cpos.index+cpos[0].length ),
				"mid_part" : r
			});

			// replace the expression with the result
			ret = ret.substr(0, cpos.index)+r+ret.substr(cpos.index+cpos[0].length );
		}
	} while (cpos !== null);

	// Multiplications and divisions
	do {
		// Get the fist expression
		cpos = RegExp(n1+'\\s*([*/%\\\\])\\s*'+n2).exec(ret);
		r = '';
		if ( cpos !== null ) { // if expression is found

			// get first number
			cn1 = cpos.groups.number1.trim().replace(/^(-{2})+/,'');

			// get second number
			cn2 = cpos.groups.number2.trim().replace(/^(-{2})+/,'');

			if ( cpos[2] == '*' ) {
				// expression is multiplication
				r = (parseFloat(cn1) * parseFloat(cn2))+'';
			}
			else if ( cpos[2] == '/' ) {
				// expression is devision
				r = (parseFloat(cn1) / parseFloat(cn2))+'';
			}
			else if ( cpos[2] == '%' ) {
				// expression is devision remainder
				r = (parseFloat(cn1) % parseFloat(cn2))+'';
			}
			else if ( cpos[2] == '\\' ) {
				// expression is integer devision
				r = Math.floor(parseFloat(cn1) / parseFloat(cn2))+'';
			}

			// Fire event.
			lvzmath.fireEvent('step', {
				"error" : false,
				"step_type" : "multiplications and divisions",
				"full_expression" : str,
				"expression" : ret,
				"action" : cpos[2],
				"number1" : cpos.groups.number1,
				"number2" : cpos.groups.number2,
				"left_part" : ret.substr(0, cpos.index),
				"right_part" : ret.substr(cpos.index+cpos[0].length ),
				"mid_part" : r
			});

			// replace the expression with the result
			ret = ret.substr(0, cpos.index)+r+ret.substr(cpos.index+cpos[0].length );
		}
	} while (cpos !== null);

	// Additions and subtractions
	do {
		// Get the first expression
		cpos = RegExp(n1+'\\s*([+\\-])\\s*'+n2).exec(ret);

		r = '';
		if ( cpos !== null ) { // if expression is found

			// get first number
			cn1 = cpos.groups.number1.trim().replace(/^(-{2})+/,'');

			// get second number
			cn2 = cpos.groups.number2.trim().replace(/^(-{2})+/,'');

			if ( cpos[2] == '+' ) {
				// expression is addition
				r = (parseFloat(cn1) + parseFloat(cn2))+'';
			}
			else if ( cpos[2] == '-' ) {
				// expression is subtraction
				r = (parseFloat(cn1) - parseFloat(cn2))+'';
			}

			// Fire event.
			lvzmath.fireEvent('step', {
				"error" : false,
				"step_type" : "additions and subtractions",
				"full_expression" : str,
				"expression" : ret,
				"action" : cpos[2],
				"number1" : cpos.groups.number1,
				"number2" : cpos.groups.number2,
				"left_part" : ret.substr(0, cpos.index),
				"right_part" : ret.substr(cpos.index+cpos[0].length ),
				"mid_part" : r
			});

			// replace the expression with the result
			ret = ret.substr(0, cpos.index)+r+ret.substr(cpos.index+cpos[0].length );
		}
	} while (cpos !== null);

	// return result.
	return ret;
};

/**
 * Static function for getting the values of ranges for use in loops.
 * @param {String} str - The range expression.
 * @return {Array|{b:number,e:number,s:false|number}} Returns an array with the values of the range or
 * an object with properties, the beginning, the end and the step of
 * the range.
 */
lvzmath.range = function ( str, v ) {
	var ret = {
		b : false,
		e : false,
		s : false
	};
	var n = '-?[0-9]+(?:[.][0-9]+)?';
	var m = str.match(RegExp('^\\s*('+n+')\\s*[.][.]\\s*('+n+')(?:\\s*[.][.]('+n+'))?\\s*$','m'));
	if ( m !== null ) {
		ret.b = parseFloat(m[1]);
		ret.e = parseFloat(m[2]);
		if ( typeof m[3] !== 'undefined' ) ret.s = parseFloat(m[3]);
		return ret;
	}

	m = str.match(/^\s*\[\s*([^,]+),\s*([^\]]+)\]\s*$/m);
	if ( m !== null ) {
		ret.b = lvzmath(m[1],v).result;
		ret.e = lvzmath(m[2],v).result;
		return ret;
	}

	m = str.match(RegExp('^\\s*[{]\\s*('+n+'(?:\\s*,\\s*'+n+')*)\\s*[}]\\s*$','m'));
	if ( m !== null ) {
		ret = m[1].split(/,/);
		return ret.map(x=>parseFloat(x));
	}
	return lvzmath(str,v).result.split(/,/).map(x=>parseFloat(x));

};

/**
 * Function for converting lvzmath strings to mathml.
 * @param {String} str - the lvzmath string.
 * @return {String} The mathml string.
 */
lvzmath.math2mathml = function ( str ) {

	function getTagB ( str ) {
		var tag = '';
		var ret = str;
		var pos = RegExp('</([a-z]+)(?: [^>]*)?>\\s*$','m').exec(ret);
		var cnt = 1;
		var lp = 0;
		var j = 0;

		if ( pos === null ) return false;
		ret = ret.substr(0,pos.index );
		tag = pos[1];
		while ( (pos = RegExp('<(/?)([A-Za-z\\-]+)( [^>]*)?>[^<]*$','m').exec(ret)) !== null ) {
			j ++;
			if ( j > 100) break;
			
			if ( pos[2] != tag ) {
				ret = ret.substr(0,pos.index);
				continue;
			}
			else if ( pos[1] == '/' ) {
				cnt ++;
				ret = ret.substr(0,pos.index);
			}
			else {
				cnt --;
				if ( cnt == 0 ) return [str.substr(0,pos.index ), str.substr(pos.index )]
				ret = ret.substr(0,pos.index);
			}
		}
		// return [str.substr(0,pos), str.substr(pos)];
		return false;
	}

	function getTagF ( str ) {
		var tag = '';
		var ret = str;
		var pos = RegExp('^\\s*<([A-Za-z\\-]+)(?: [^>]*)?>','m').exec(ret);
		var cnt = 1;
		var lp = 0;
		var j = 0;

		if ( pos === null ) return false;

		ret = ret.substr(pos.index + pos[0].length );
		tag = pos[1];

		while ( (pos = RegExp('^[^<]*<(/?)([A-Za-z\\-]+)(?: [^>]*)?>','m').exec(ret)) !== null ) {
			j ++;
			if ( j > 100) break;

			if ( pos[2] != tag ) {
				ret = ret.substr(pos.index + pos[0].length);
				continue;
			}
			else if ( pos[1] == '/' ) {
				cnt --;				
				if ( cnt == 0 ) return [str.substr(0, str.length  - ret.length) + pos[0], ret.substr(pos[0].length)];
				ret = ret.substr(pos.index + pos[0].length);
			}
			else {
				cnt ++;
				ret = ret.substr(pos.index + pos[0].length);
			}
		}
		if ( pos === null ) return false;
		return [str.substr(0, str.length  - ret.length) + pos[0], ret.substr(pos[0].length)];
	}

	function getBracketLeft ( str, bopen, bclose ) {
		var points=1;
		for ( var i=str.length-1; i > -1; i -- ) {
			if ( str.charAt(i) == bclose) points++;
			else if ( str.charAt(i) == bopen) points--;

			if ( points === 0 ) return str.substring(i+1);
		}
		return false;
	}

	function getBracketRight ( str, bopen, bclose ) {
		var points=1;
		for ( var i=0; i< str.length; i ++) {
			if ( str.charAt(i) == bopen) points++;
			else if ( str.charAt(i) == bclose) points--;
			if ( points === 0 ) return str.substring(0,i);
		}
		return false;
	}

	var ret = '('+str+')';
	var n1 = '(-?[0-9]+(?:[.][0-9]+)?)';

	ret = ret
		.replace(/(?<![*])[*](?![*])/gm, "×") // Asterisk to times
		.replace(/[+]-/gm,'<mo>±</mo>')
		.replace(/-[+]/gm,'<mo>∓</mo>')
		.replace(/[<][=]/gm,'<mo>&le;</mo>') // ≤	&le;
		.replace(/[>][=]/gm,'<mo>&ge;</mo>') // ≥	&ge;

	// Integer division
		.replace(/(\)|\b(?:-?[0-9]+(?:[.][0-9]+)?)\b|\b[A-Za-z][A-Za-z0-9]+\b)\s*[\\]\s*(\(|\b(?:-?[0-9]+(?:[.][0-9]+)?)\b|\b[A-Za-z][A-Za-z0-9]+\b)/gm,function(m,c,o,i){
			var ostr = arguments[arguments.length-1];
			var pos = arguments[arguments.length-2];

			var p1 = '';
			var p2 = '';
			if ( arguments[1] == ')' ) {
				arguments[1]=getBracketLeft(ostr.substring(0,pos),'(',')');
				p1 = '<mrow>'+arguments[1]+'</mrow>';
			}
			else {
				p1 = arguments[1]
					.replace(/^([a-zA-Z][a-zA-Z0-9]+)$/gm, "<mi>$1</mi>") // Move variable names in "mi" tags
					.replace(/^(-?[0-9]+(?:[.][0-9]+)?)$/gm, "<mn>$1</mn>") // Move numbers in "mn" tags
				;
			}

			if ( arguments[2] == '(' ) {
				arguments[2] = getBracketRight(ostr.substring(pos + m.length ),'(',')');
				p2 = '<mrow>'+arguments[2]+'</mrow>';
			}
			else {
				p2 = arguments[2]
					.replace(/^([a-zA-Z][a-zA-Z0-9]+)$/gm, "<mi>$1</mi>") // Move variable names in "mi" tags
					.replace(/^(-?[0-9]+(?:[.][0-9]+)?)$/gm, "<mn>$1</mn>") // Move numbers in "mn" tags
				;
			}

			console.log ( '---------------------------------------------')
			console.log ( p1 );
			console.log ( p2 );
			console.log ( '---------------------------------------------')
			//'<mo>&lfloor;</mo><mfrac><mi>$1</mi><mi>$2</mi></mfrac><mo>&rfloor;</mo>'
			return '&lfloor;<mfrac>'+p1+p2+'</mfrac>&rfloor;';
		})

	// .replace(/\b([a-zA-Z][a-zA-Z0-9_]*)\b/gm, "<mi>$1</mi>") // Move variable names in "mi" tags
		.replace(/\b(?<![<&])([a-zA-Z][a-zA-Z0-9_]+)(?![>;])\b/gm, "<mi>$1</mi>") // Move variable names in "mi" tags
		.replace(/\b(-?[0-9]+(?:[.][0-9]+)?)\b/gm, "<mn>$1</mn>") // Move numbers in "mn" tags
		.replace(/([0-9a-zA-Z()>]|[&][lr]floor[;])\s*([+\-×±∓])\s*([<0-9a-zA-Z()\-])/gm,'$1<mo>$2</mo>$3') // Move operators in "mo" tags
	// .replace(/pi([(][)])?/gm,"<mi>&#x03C0;</mi>") // Convert "pi" constant or function to π
		.replace(/pi([(][)])?/gm,"<mi>&pi;</mi>") // Convert "pi" constant or function to π

		.replace(/[(]/gm,'<mrow>')
		.replace(/[)]/gm,'</mrow>')
		.replace(/\n/gm,'')		// Remove new lines
	;

	var cpos;
	var j = 0;

	// Superscripts roots and fractions
	do {
		cpos = RegExp(/[*]{2}|[/]{2}|\^|(?<![<])[/]|[%]/gm).exec(ret);

		if ( cpos === null ) break;

		var fp =ret.substr(0,cpos.index);
		var ap =ret.substr(cpos.index + cpos[0].length);
		var tn = fp.match(/<\/([A-Za-z0-9\-\_]+)>\s*$/m);

		if ( cpos[0] == '/' ) { // Fractions
			if ( tn !== null ) {
				fp1 = getTagB ( fp );
				ap1 = getTagF ( ap );
				if ( fp1 !== false && ap1 !== false ) {
					ret = fp1[0] + '<mfrac>' + fp1[1] + ap1[0] + '</mfrac>' + ap1[1];
				}
				else {
					console.log ( 'error');
					break;
				}
			}
		}
		else if ( cpos[0] == '%' ) { // Division modulo
			if ( tn !== null ) {
				fp1 = getTagB ( fp );
				ap1 = getTagF ( ap );
				if ( fp1 !== false && ap1 !== false ) {
					fp1[1] = fp1[1]
						.replace(/^\s*[<]mrow[>]/gm,"<mrow><mo fence=\"true\" stretchy=\"false\">(</mo>")
						.replace(/[<][/]mrow[>]\s*$/gm,"<mo fence=\"true\" stretchy=\"false\">)</mo></mrow>")
					;
					ap1[0] = ap1[0]
						.replace(/^\s*[<]mrow[>]/gm,"<mrow><mo fence=\"true\" stretchy=\"false\">(</mo>")
						.replace(/[<][/]mrow[>]\s*$/gm,"<mo fence=\"true\" stretchy=\"false\">)</mo></mrow>")
					;
					ret = fp1[0] + '<mrow>' + fp1[1] + '</mrow><mo>mod</mo><mrow>' + ap1[0] + '</mrow>' + ap1[1];
				}
				else {
					console.log ( 'error' );
					break;
				}
			}
		}
		else if ( cpos[0] == '**' || cpos[0] == '^' ) { // Powers
			if ( tn !== null ) {
				fp1 = getTagB( fp );
				ap1 = getTagF( ap );
				if ( fp1 !== false && ap1 !== false ) {
					fp1[1] = fp1[1]
						.replace(/^\s*[<]mrow[>]/gm,"<mrow><mo fence=\"true\" stretchy=\"false\">(</mo>")
						.replace(/[<][/]mrow[>]\s*$/gm,"<mo fence=\"true\" stretchy=\"false\">)</mo></mrow>")
					;
					ap1[0] = ap1[0]
						.replace(/^\s*[<]mrow[>]/gm,"<mrow><mo fence=\"true\" stretchy=\"false\">(</mo>")
						.replace(/[<][/]mrow[>]\s*$/gm,"<mo fence=\"true\" stretchy=\"false\">)</mo></mrow>")
					;
					ret = fp1[0] + '<msup>' + fp1[1] + ap1[0] + '</msup>' + ap1[1];
				}
			}
		}
		else if ( cpos[0] == '//' ) { // Roots
			if ( tn !== null ) {
				fp1 = getTagB ( fp );
				ap1 = getTagF ( ap );
				if ( fp1 !== false && ap1 !== false ) {
					if ( ap1[0].trim() == '<mn>2</mn>' ) {
						ret = fp1[0] + '<msqrt>' + fp1[1] + '</msqrt>' + ap1[1];
					}
					else {
						ret = fp1[0] + '<mroot>' + fp1[1] + ap1[0] + '</mroot>' + ap1[1];
					}
				}
				else {
					console.log ( 'error' );
					break;
				}
			}
		}

		if ( j > 1000 )break;
		j ++;

	} while ( cpos !== null );

	if ( j > 1000 ) {
		console.log ( j );
		console.log ( ret ) ;
	}

	j = 0;
	var tmpret = ret;
	ret='';
	// Functions
	do {

		cpos = RegExp(/((<mi>([A-Za-z]+[0-9]*)<[/]mi>)|([A-Za-z]+[0-9]*))\s*(<mrow[^>]*>)/gm).exec(tmpret);
		if ( cpos === null ) break;
		var ap = tmpret.substr(cpos.index + (cpos[0].length-cpos[5].length));

		if ( typeof cpos[3] != 'undefined' ) cpos[0] = cpos[3];
		else if ( typeof cpos[4] != 'undefined' ) cpos[0] = cpos[4];

		// if ( ['sin','cos','abs','sqrt','root'].indexOf(cpos[0]) > -1 ) {
		if ( ap !== false ) {
			ap1 = getTagF ( ap ) ;
			console.log ( ap1 );
			if ( ap1 !== false ) {
				ap1[0] = ap1[0]
					.replace(/^\s*[<]mrow[>]/gm,"<mrow><mo fence=\"true\" stretchy=\"false\">(</mo>")
					.replace(/[<][/]mrow[>]\s*$/gm,"<mo fence=\"true\" stretchy=\"false\">)</mo></mrow>")
				;
				ret += tmpret.substr(0,cpos.index) + '<mi>'+cpos[0]+'</mi><mrow>'+ap1[0] +'</mrow>';
				tmpret = ap1[1];
			}
			else {
				ret += tmpret.substr(0,cpos.index + cpos[0].length);
				tmpret=tmpret.substr(cpos.index + cpos[0].length);
			}
		}
		else {
			ret += tmpret.substr(0,cpos.index + cpos[0].length);
			tmpret=tmpret.substr(cpos.index + cpos[0].length);
		}
		//}

		if ( j > 1000 )break;
		j ++ ;

	} while ( cpos !== null );

	ret+=tmpret;

	if ( j > 1000 ) {
		console.log ( j );
		console.log ( ret ) ;
	}


	return ret;
};

/**
 * Function for solving equations.
 * @param {String} str - The equation string.
 * @param {Object} variables - Object with variables with known values.
 * @return {Object} Object with the solution for each unknown variable.
 */
lvzmath.equation = function ( str, variables ) {

	function reverseSigns ( str ) {
		return str.replace(/[\-+*/]/mg,function(m) {
			if (m[0] == '-' ) return '+';
			if (m[0] == '+' ) return '-';
			if (m[0] == '*' ) return '/';
			if (m[0] == '/' ) return '*';
		});
	}

	function getVariablePart ( str ) {
		return str.match(/[a-zA-Z]+[a-zA-Z0-9]*(?:(?:\\s*[+\-*/][a-zA-Z]+[a-zA-Z0-9]*)+)?/);
	}

	function replaceVariablePart ( str, withStr ) {
		return str.replace(/[a-zA-Z]+[a-zA-Z0-9]*(?:(?:\\s*[+\-*/][a-zA-Z]+[a-zA-Z0-9]*)+)?/,withStr);
	}

	if ( typeof variables != 'object' ) variables = [];

	for ( var i in variables )
		str = str.replace(RegExp(i,'mg'),variables[i].value);

	var parts = str.split(/=/);

	var ret = '';

	if ( parts.length == 2 ) {
		// 2 - x * 5 = 4 * 5 * 4
		// (2 - (4 * 4 * 5)) / 5 = -15.6
		// x = -15.6

		// 2 + x * 5 =  4 * 5 * 4
		// ((4 * 5 * 4) - 2) / 5 = 15.6
		// x = 15.6

		var p1 = lvzmath.evaluate(parts[0]);
		var p2 = lvzmath.evaluate(parts[1]);
		var vp1 = getVariablePart ( reverseSigns ( lvzmath.evaluate(p1) ) );
		var vp2 = getVariablePart ( reverseSigns ( lvzmath.evaluate(p2) ) );

		if ( vp1 === null && vp2 !== null ) {
			ret += lvzmath.evaluate( vp2[0] + ' = ' + replaceVariablePart ( reverseSigns ( lvzmath.evaluate(p2) ) , p1 ) );
		}
		else if ( vp1 !== null && vp2 === null ) {
			ret += lvzmath.evaluate( vp1[0] + ' = ' + replaceVariablePart ( reverseSigns ( lvzmath.evaluate(p1) ) , p2 ) );
		}

	}

	return ret;

};

/**
 * Function for detecting the language of a string.
 * @param {String} s - The string to check.
 * @return {String} Two letter language code or empty string in not found.
 */
lvzmath.detectLang=function(s){
	var lang={},ret='',mx=0;
	function ck(m){lang[l]++;return '';}
	for(var l in lvzmath.regex){lang[l]=0;for(var i in lvzmath.regex[l])s.replace(RegExp(i,'igmus'),ck);}
	for(var l in lang)if(lang[l]>mx){mx=lang[l];ret=l;}
	return ret;
};

/**
 * Function for translating words representing numbers into numbers.
 * @param {String} s - The string to search to search for words
 * representing numbers and translate them to numbers.
 * @param {String} l - The language code of the language of the
 * string.
 * @return {String} The translated string.
 */
lvzmath.numberFromString=function(s,l){
	var ret=s;
	if(typeof l=='string'&&typeof lvzmath.regex[l]!='undefined')for(var i in lvzmath.regex[l])ret=ret.replace(RegExp(i,'igmus'),lvzmath.regex[l][i]);
	if(typeof l=='boolean'&&l===true)for(var ln in lvzmath.regex)for(var i in lvzmath.regex[ln])ret=ret.replace(RegExp(i,'igmus'),lvzmath.regex[ln][i]);
	return ret;
};

/**
 * Function for adding events.
 * @param {String} e - The name of the event or a comma separated of the events.
 * @param {Function} f - The function to call when the event fires.
 * @return {boolean} True on success, false on error.
 */
lvzmath.addEvent = function ( e, f ) {
	if ( typeof e != 'string' ) return false;
	if ( typeof f != 'function') return false;
	e=e.split(/,/);
	for (var i=0;i<e.length;i++){
		e[i]=e[i].trim();
		if ( typeof lvzmath['on'+e[i]] == 'undefined' ) lvzmath['on'+e[i]] = [];
		if ( lvzmath['on'+e[i]].indexOf(f) > -1 ) continue;
		lvzmath['on'+e[i]].push(f);
	}
	return true;
};

/**
 * Function for removing events.
 * @param {String} e - The name of the event or a comma separated of the events.
 * @param {Function} [f] - The function to remove. If this is not set then all the functions will be removed.
 * @return {boolean} True on success, false on error.
 */
lvzmath.removeEvent = function ( e, f ) {
	if ( typeof e != 'string' ) return false;
	if ( typeof f != 'undefined' && typeof f != 'function') return false;
	e=e.split(/,/);
	for(var i=0;i<e.length;i++){
		e[i]=e[i].trim();
		if ( typeof lvzmath['on'+e[i]] == 'undefined' ) continue;
		if ( typeof f == 'undefined' ) {
			lvzmath['on'+e[i]] = [];
		}
		else {
			var mf = lvzmath['on'+e[i]].indexOf(f);
			if ( mf === -1 ) return true;
			lvzmath['on'+e[i]].splice(mf, 1);
		}
	}
	return true;
};

/**
 * Function for firing events.
 * @param {String} e - The name of the event or a comma separated of the events to be fired.
 * @param {Object} data - The events data.
 * @return {boolean} True on success, false on error.
 */
lvzmath.fireEvent = function ( e, data ) {
	e=e.split(/,/);
	for(var i=0;i<e.length;i++){
		e[i]=e[i].trim();
		if (typeof lvzmath['on'+e[i]] == 'undefined' ) continue;
		data.type = e[i];
		for ( var j=0; j<lvzmath['on'+e[i]].length; j++ )
			lvzmath['on'+e[i]][j]( data );
	}
	return true;
};

/**
 * Function for getting the first most inner parenthesis in the given string.
 * @param {String} str - The expression to search for the parenthesis.
 */
lvzmath.getFirstMostInnerParenthesis = function ( str ) {
	var ppos = str.indexOf(')');
	if ( ppos === -1 ) return false;
	var mstr = str.substr(0, ppos);
	var ppos1 = mstr.lastIndexOf('(');
	if ( ppos === -1 ) return false;
	return {
		"start" : ppos1,
		"end" : ppos,
		"left" : str.substr(0,ppos1),
		"middle" : mstr.substr(ppos1+1),
		"right" : str.substr(ppos+1)
	};

};

/**
 * Property for activating/deactivating mathematical functions.
 * @type {boolean}
 */
lvzmath.useFunctions = true;

/**
 * Property for activating/deactivating user defined variables.
 * @type {boolean}
 */
lvzmath.useVariables = true;

/**
 * Property for activating/deactivating predefined constants.
 * @type {boolean}
 */
lvzmath.useConstants = true;

/**
 * Property for activating/deactivating user defined functions.
 * @type {boolean}
 */
lvzmath.useCustomFunctions = true;

/**
 * Array for storing the names of user defined functions.
 * @type {Array}
 */
lvzmath.customFunctionNames = [];

/**
 * Object for storing user defined function data.
 * @type {Object}
 */
lvzmath.customFunctions = {};

/**
 * Property for storing the constants.
 * @type {Object}
 */
lvzmath.constants = {
	'e'        : Math.E,
	'ln10'     : Math.LN10,
	'ln2'      : Math.LN2,
	'log10e'   : Math.LOG10E,
	'log2e'    : Math.LOG2E,
	'pi'       : Math.PI,
	'π'        : Math.PI,
	'sqrt1_2'  : Math.SQRT1_2,
	'sqrt2'    : Math.SQRT2,
	'Infinity' : Infinity,
	'NaN'      : NaN
};

/**
 * Property for storing the reserved names of functions and constants.
 * @type {Array}
 */
lvzmath.reservedNames = ['pi','random','abs','acos','acosh','asin','asinh',
						 'atan','atanh','ceil','cbrt','cos','cosh','exp',
						 'expm1','floor','log','log10','log1p','log2','root',
						 'round','sign','sin','sinh','sqrt','tan','tanh',
						 'trunc','pow','atan2','max','min','e','ln10','ln2',
						 'log10e','log2e','sqrt1_2','sqrt2','π','Infinity','NaN',
						 'sum','avg','median','mode','range','count','left',
						 'right','mid','replace','indexof','list','item',
						 'seq','fibonacci','fseq','reverse','sort','first','last',
						 'lroll','rroll','factors','factorial','antifactorial'];


/**
 * Property for storing regular expressions for turning words in numbers.
 * @type {object}
 */
lvzmath.regex = {
	"el":{
		"(?:(?:εκατ[όο](ν)?)(?!μμ[υύ]ρι[οα]))([\\s]*)([εέ]να|δ[ύυ]ο|τρ(?:[ιί]α(?!ντα)|ε[ιί]ς)|τ[εέ]σσ?ερ(?:α|ε[ιί]ς)|π[εέ]ντε|[εέ]ξι|ε[πφ]τ[αά]|ο[χκ]τ[ωώ]|ενν([εέ]α|ι[αά]))":'10$3',
		"(?:(?:εκατ[όο](ν)?)(?!μμ[υύ]ρι[οα]))([\\s]*)(δ[έε]κα|[εέ]ν[δτ]εκα|δ[ωώ]δεκα|ε[ίι]κοσι|τρι[άα]ντα|σαρ[άα]ντα|πεν[ήη]ντα|εξ[ήη]ντα|εβδομ[ήη]ντα|ογδ[όο]ντα|ενεν[ήη]ντα)":'1$3',

		"διακ[όο]σι(α|ες)([\\s]*)([εέ]να|δ[ύυ]ο|τρ(?:[ιί]α(?!ντα)|ε[ιί]ς)|τ[εέ]σσ?ερ(?:α|ε[ιί]ς)|π[εέ]ντε|[εέ]ξι|ε[πφ]τ[αά]|ο[χκ]τ[ωώ]|ενν([εέ]α|ι[αά]))":'20$3',
		"διακ[όο]σι(α|ες)([\\s]*)(δ[έε]κα|[εέ]ν[δτ]εκα|δ[ωώ]δεκα|ε[ίι]κοσι|τρι[άα]ντα|σαρ[άα]ντα|πεν[ήη]ντα|εξ[ήη]ντα|εβδομ[ήη]ντα|ογδ[όο]ντα|ενεν[ήη]ντα)":'2$3',

		"τριακ[όο]σι(α|ες)([\\s]*)([εέ]να|δ[ύυ]ο|τρ(?:[ιί]α(?!ντα)|ε[ιί]ς)|τ[εέ]σσ?ερ(?:α|ε[ιί]ς)|π[εέ]ντε|[εέ]ξι|ε[πφ]τ[αά]|ο[χκ]τ[ωώ]|ενν([εέ]α|ι[αά]))":'30$3',
		"τριακ[όο]σι(α|ες)([\\s]*)(δ[έε]κα|[εέ]ν[δτ]εκα|δ[ωώ]δεκα|ε[ίι]κοσι|τρι[άα]ντα|σαρ[άα]ντα|πεν[ήη]ντα|εξ[ήη]ντα|εβδομ[ήη]ντα|ογδ[όο]ντα|ενεν[ήη]ντα)":'3$3',

		"τετρακ[όο]σι(α|ες)([\\s]*)([εέ]να|δ[ύυ]ο|τρ(?:[ιί]α(?!ντα)|ε[ιί]ς)|τ[εέ]σσ?ερ(?:α|ε[ιί]ς)|π[εέ]ντε|[εέ]ξι|ε[πφ]τ[αά]|ο[χκ]τ[ωώ]|ενν([εέ]α|ι[αά]))":'40$3',
		"τετρακ[όο]σι(α|ες)([\\s]*)(δ[έε]κα|[εέ]ν[δτ]εκα|δ[ωώ]δεκα|ε[ίι]κοσι|τρι[άα]ντα|σαρ[άα]ντα|πεν[ήη]ντα|εξ[ήη]ντα|εβδομ[ήη]ντα|ογδ[όο]ντα|ενεν[ήη]ντα)":'4$3',

		"πεντακ[όο]σι(α|ες)([\\s]*)([εέ]να|δ[ύυ]ο|τρ(?:[ιί]α(?!ντα)|ε[ιί]ς)|τ[εέ]σσ?ερ(?:α|ε[ιί]ς)|π[εέ]ντε|[εέ]ξι|ε[πφ]τ[αά]|ο[χκ]τ[ωώ]|ενν([εέ]α|ι[αά]))":'50$3',
		"πεντακ[όο]σι(α|ες)([\\s]*)(δ[έε]κα|[εέ]ν[δτ]εκα|δ[ωώ]δεκα|ε[ίι]κοσι|τρι[άα]ντα|σαρ[άα]ντα|πεν[ήη]ντα|εξ[ήη]ντα|εβδομ[ήη]ντα|ογδ[όο]ντα|ενεν[ήη]ντα)":'5$3',

		"εξακ[όο]σι(α|ες)([\\s]*)([εέ]να|δ[ύυ]ο|τρ(?:[ιί]α(?!ντα)|ε[ιί]ς)|τ[εέ]σσ?ερ(?:α|ε[ιί]ς)|π[εέ]ντε|[εέ]ξι|ε[πφ]τ[αά]|ο[χκ]τ[ωώ]|ενν([εέ]α|ι[αά]))":'60$3',
		"εξακ[όο]σι(α|ες)([\\s]*)(δ[έε]κα|[εέ]ν[δτ]εκα|δ[ωώ]δεκα|ε[ίι]κοσι|τρι[άα]ντα|σαρ[άα]ντα|πεν[ήη]ντα|εξ[ήη]ντα|εβδομ[ήη]ντα|ογδ[όο]ντα|ενεν[ήη]ντα)":'6$3',

		"ε[πφ]τακ[όο]σι(α|ες)([\\s]*)([εέ]να|δ[ύυ]ο|τρ(?:[ιί]α(?!ντα)|ε[ιί]ς)|τ[εέ]σσ?ερ(?:α|ε[ιί]ς)|π[εέ]ντε|[εέ]ξι|ε[πφ]τ[αά]|ο[χκ]τ[ωώ]|ενν([εέ]α|ι[αά]))":'70$3',
		"ε[πφ]τακ[όο]σι(α|ες)([\\s]*)(δ[έε]κα|[εέ]ν[δτ]εκα|δ[ωώ]δεκα|ε[ίι]κοσι|τρι[άα]ντα|σαρ[άα]ντα|πεν[ήη]ντα|εξ[ήη]ντα|εβδομ[ήη]ντα|ογδ[όο]ντα|ενεν[ήη]ντα)":'7$3',

		"ο[χκ]τακ[όο]σι(α|ες)([\\s]*)([εέ]να|δ[ύυ]ο|τρ(?:[ιί]α(?!ντα)|ε[ιί]ς)|τ[εέ]σσ?ερ(?:α|ε[ιί]ς)|π[εέ]ντε|[εέ]ξι|ε[πφ]τ[αά]|ο[χκ]τ[ωώ]|ενν([εέ]α|ι[αά]))":'80$3',
		"ο[χκ]τακ[όο]σι(α|ες)([\\s]*)(δ[έε]κα|[εέ]ν[δτ]εκα|δ[ωώ]δεκα|ε[ίι]κοσι|τρι[άα]ντα|σαρ[άα]ντα|πεν[ήη]ντα|εξ[ήη]ντα|εβδομ[ήη]ντα|ογδ[όο]ντα|ενεν[ήη]ντα)":'8$3',

		"εννιακ[όο]σι(α|ες)([\\s]*)([εέ]να|δ[ύυ]ο|τρ(?:[ιί]α(?!ντα)|ε[ιί]ς)|τ[εέ]σσ?ερ(?:α|ε[ιί]ς)|π[εέ]ντε|[εέ]ξι|ε[πφ]τ[αά]|ο[χκ]τ[ωώ]|ενν([εέ]α|ι[αά]))":'90$3',
		"εννιακ[όο]σι(α|ες)([\\s]*)(δ[έε]κα|[εέ]ν[δτ]εκα|δ[ωώ]δεκα|ε[ίι]κοσι|τρι[άα]ντα|σαρ[άα]ντα|πεν[ήη]ντα|εξ[ήη]ντα|εβδομ[ήη]ντα|ογδ[όο]ντα|ενεν[ήη]ντα)":'9$3',

		"μηδ[έε]ν":'0',"[έε]ν['α]":'1',"δ[ύυ]ο":'2',"τρ(?:[ιί]α(?!ντα)|ε[ιί]ς)":'3',"τ[εέ]σσ?ερ(?:α|ε[ιί]ς)":'4',"π[εέ]ντε":'5',"[εέ]ξι":'6',"ε[πφ]τ[αά]":'7',"ο[χκ]τ[ωώ]":'8',"ενν([εέ]α|ι[αά])":'9',

		"δ[έε]κα([\\s]*)([3-9])":'1$2',
		"ε[ίι]κοσι([\\s]*)([1-9])":'2$2',
		"τρι[άα]ντα([\\s]*)([1-9])":'3$2',
		"σαρ[άα]ντα([\\s]*)([1-9])":'4$2',
		"πεν[ήη]ντα([\\s]*)([1-9])":'5$2',
		"εξ[ήη]ντα([\\s]*)([1-9])":'6$2',
		"εβδομ[ήη]ντα([\\s]*)([1-9])":'7$2',
		"ογδ[όο]ντα([\\s]*)([1-9])":'8$2',
		"ενεν[ήη]ντα([\\s]*)([1-9])":'9$2',

		"δ[έε]κα":'10',"[έε]ν(([δτ]εκα)|10)":'11',"δ[ώω]10":'12',"ε[ίι]κοσι":'20',"τρι[άα]ντα":'30',"σαρ[άα]ντα":'40',"πεν[ήη]ντα":'50',"εξ[ήη]ντα":'60',"εβδομ[ήη]ντα":'70',"ογδ[όο]ντα":'80',"ενεν[ήη]ντα":'90',
		"εκατ[οό](?!μμ[υύ]ρι[οα])":'100',"διακ[οό]σι(α|ες)":'200',"τριακ[οό]σι(α|ες)":'300',"τετρακ[οό]σι(α|ες)":'400',"πεντακ[οό]σι(α|ες)":'500',"εξακ[οό]σι(α|ες)":'600',"ε[πφ]τακ[οό]σι(α|ες)":'700',"ο[χκ]τακ[οό]σι(α|ες)":'800',"εννιακ[οό]σι(α|ες)":'900',

		"χ[ίι]λια(?!δες)(\\s*)([0-9]{3})":'1$2',
		"χ[ίι]λια(?!δες)(\\s*)([0-9]{2})":'10$2',
		"χ[ίι]λια(?!δες)(\\s*)([0-9]{1})":'100$2',
		"χ[ίι]λια(?!δες)":'1000',

		"([0-9]{1,3})\\s*χιλι[άα]δες\\s*([0-9]{3})":'$1$2',
		"(([0-9]{1,3})(\\s*)χιλι[άα]δες)(\\s*)([0-9]{2})":'$20$5',
		"(([0-9]{1,3})(\\s*)χιλι[άα]δες)(\\s*)([0-9])":'$200$5',
		"(([0-9]{1,3})(\\s*)χιλι[άα]δες)":'$2000',

		"([0-9]{1,3})(\\s*)εκατομμ[ύυ]ρι[οα](\\s*)([0-9]{6})":'$1$4',"([0-9]{1,3})(\\s*)εκατομμ[ύυ]ρι[οα](\\s*)([0-9]{5})":'$10$4',"([0-9]{1,3})(\\s*)εκατομμ[ύυ]ρι[οα](\\s*)([0-9]{4})":'$100$4',"([0-9]{1,3})(\\s*)εκατομμ[ύυ]ρι[οα](\\s*)([0-9]{3})":'$1000$4',"([0-9]{1,3})(\\s*)εκατομμ[ύυ]ρι[οα](\\s*)([0-9]{2})":'$10000$4',"([0-9]{1,3})(\\s*)εκατομμ[ύυ]ρι[οα](\\s*)([0-9]{1})":'$100000$4',"([0-9]{1,3})(\\s*)εκατομμ[ύυ]ρι[οα]":'$1000000',
		"(?:[^0-9α-ω])εκατομμ[ύυ]ριο(\\s*)([0-9]{6})":'1$2',"(?:[^0-9α-ω])εκατομμ[ύυ]ριο(\\s*)([0-9]{5})":'10$2',"(?:[^0-9α-ω])εκατομμ[ύυ]ριο(\\s*)([0-9]{4})":'100$2',"(?:[^0-9α-ω])εκατομμ[ύυ]ριο(\\s*)([0-9]{3})":'1000$2',"(?:[^0-9α-ω])εκατομμ[ύυ]ριο(\\s*)([0-9]{2})":'10000$2',"(?:[^0-9α-ω])εκατομμ[ύυ]ριο(\\s*)([0-9])":'100000$2',"(?:[^0-9α-ω])εκατομμ[ύυ]ριο":'1000000',
		"([0-9]{1,3})(\\s*)δισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{9})":'$1$4',"([0-9]{1,3})(\\s*)δισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{8})":'$10$4',"([0-9]{1,3})(\\s*)δισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{7})":'$100$4',"([0-9]{1,3})(\\s*)δισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{6})":'$1000$4',"([0-9]{1,3})(\\s*)δισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{5})":'$10000$4',"([0-9]{1,3})(\\s*)δισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{4})":'$100000$4',"([0-9]{1,3})(\\s*)δισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{3})":'$1000000$4',"([0-9]{1,3})(\\s*)δισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{2})":'$10000000$4',"([0-9]{1,3})(\\s*)δισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{1})":'$100000000$4',"([0-9]{1,3})(\\s*)δισεκατομμ[ύυ]ρι[οα]":                '$1000000000',
		"δισεκατομμ[ύυ]ριο(\\s*)([0-9]{9})":'1$2',"δισεκατομμ[ύυ]ριο(\\s*)([0-9]{8})":'10$2',"δισεκατομμ[ύυ]ριο(\\s*)([0-9]{7})":'100$2',"δισεκατομμ[ύυ]ριο(\\s*)([0-9]{6})":'1000$2',"δισεκατομμ[ύυ]ριο(\\s*)([0-9]{5})":'10000$2',"δισεκατομμ[ύυ]ριο(\\s*)([0-9]{4})":'100000$2',"δισεκατομμ[ύυ]ριο(\\s*)([0-9]{3})":'1000000$2',"δισεκατομμ[ύυ]ριο(\\s*)([0-9]{2})":'10000000$2',"δισεκατομμ[ύυ]ριο(\\s*)([0-9])":   '100000000$2',"δισεκατομμ[ύυ]ριο":                '1000000000',
		"([0-9]{1,3})(\\s*)τρισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{12})":'$1$4',"([0-9]{1,3})(\\s*)τρισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{11})":'$10$4',"([0-9]{1,3})(\\s*)τρισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{10})":'$100$4',"([0-9]{1,3})(\\s*)τρισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{9})":'$1000$4',"([0-9]{1,3})(\\s*)τρισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{8})":'$10000$4',"([0-9]{1,3})(\\s*)τρισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{7})":'$100000$4',"([0-9]{1,3})(\\s*)τρισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{6})":'$1000000$4',"([0-9]{1,3})(\\s*)τρισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{5})":'$10000000$4',"([0-9]{1,3})(\\s*)τρισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{4})":'$100000000$4',"([0-9]{1,3})(\\s*)τρισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{3})":'$1000000000$4',"([0-9]{1,3})(\\s*)τρισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{2})":'$10000000000$4',"([0-9]{1,3})(\\s*)τρισεκατομμ[ύυ]ρι[οα](\\s*)([0-9]{1})":'$100000000000$4',"([0-9]{1,3})(\\s*)τρισεκατομμ[ύυ]ρι[οα]":                '$1000000000000',
		"τρισεκατομμ[ύυ]ριο(\\s*)([0-9]{12})":'1$2',"τρισεκατομμ[ύυ]ριο(\\s*)([0-9]{11})":'10$2',"τρισεκατομμ[ύυ]ριο(\\s*)([0-9]{10})":'100$2',"τρισεκατομμ[ύυ]ριο(\\s*)([0-9]{9})":'1000$2',"τρισεκατομμ[ύυ]ριο(\\s*)([0-9]{8})":'10000$2',"τρισεκατομμ[ύυ]ριο(\\s*)([0-9]{7})":'100000$2',"τρισεκατομμ[ύυ]ριο(\\s*)([0-9]{6})":'1000000$2',"τρισεκατομμ[ύυ]ριο(\\s*)([0-9]{5})":'10000000$2',"τρισεκατομμ[ύυ]ριο(\\s*)([0-9]{4})":'100000000$2',"τρισεκατομμ[ύυ]ριο(\\s*)([0-9]{3})":'1000000000$2',"τρισεκατομμ[ύυ]ριο(\\s*)([0-9]{2})":'10000000000$2',"τρισεκατομμ[ύυ]ριο(\\s*)([0-9])":   '100000000000$2',"τρισεκατομμ[ύυ]ριο":                '1000000000000',

		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{15})":'$1$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{14})":'$10$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{13})":'$100$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{12})":'$1000$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{11})":'$10000$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{10})":'$100000$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{9})":'$1000000$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{8})":'$10000000$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{7})":'$100000000$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{6})":'$1000000000$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{5})":'$10000000000$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{4})":'$100000000000$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{3})":'$1000000000000$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{2})":'$10000000000000$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)(\\s*)([0-9]{1})":'$100000000000000$4',
		"([0-9]{1,3})(\\s*)τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον|α)":                '$1000000000000000',

		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9]{15})":'1$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9]{14})":'10$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9]{13})":'100$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9]{12})":'1000$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9]{11})":'10000$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9]{10})":'100000$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9]{9})":'1000000$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9]{8})":'10000000$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9]{7})":'100000000$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9]{6})":'1000000000$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9]{5})":'10000000000$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9]{4})":'100000000000$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9]{3})":'1000000000000$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9]{2})":'10000000000000$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)(\\s*)([0-9])":   '100000000000000$2',
		"τετρ[άα]κισεκατομμ[ύυ]ρι(?:ο|ον)":                '1000000000000000',



		/*
		  τετράκισεκατομμύριο

		  τετρακισ
		  πεντακισ
		  εξακισ
		  ε[πφ]τακισ
		  οκτακισ
		  εννιάκισ
		*/
		"μισό":'1/2',
		"([0-9][\\s]*)πρ[ώω]τ[οα]":'$1/ 1',
		"([0-9][\\s]*)δε[ύυ]τερ[οα]":'$1/ 2',
		"([0-9][\\s]*)τρ[ίι]τ[οα]":'$1/ 3',
		"([0-9][\\s]*)τ[έε]ταρτ[οα]":'$1/ 4',
		"([0-9][\\s]*)π[έε]μπτ[οα]":'$1/ 5',
		"([0-9][\\s]*)[έε]κτ[οα]":'$1/ 6',
		"([0-9][\\s]*)[έε]βδομ[οα]":'$1/ 7',
		"([0-9][\\s]*)[όο]γδο[οα]":'$1/ 8',
		"([0-9][\\s]*)[εέ]νατ[οα]":'$1/ 9',
		"([0-9][\\s]*)10τ[οα]":'$1/ 10',
		"([0-9][\\s]*)εν10τ[οα]":'$1/ 11',
		"([0-9][\\s]*)δω10τ[οα]":'$1/ 12',

		"([0-9][\\s]*)εικοστ[όοάα]":'$1/ 20',
		"([0-9][\\s]*)τριακοστ[όοάα]":'$1/ 30',
		"([0-9][\\s]*)τεσσαρακοστ[όοάα]":'$1/ 40',
		"([0-9][\\s]*)πεντηκοστ[όοάα]":'$1/ 50',
		"([0-9][\\s]*)εξηκοστ[όοάα]":'$1/ 60',
		"([0-9][\\s]*)εβδομηκοστ[όοάα]":'$1/ 70',
		"([0-9][\\s]*)ογδοηκοστ[όοάα]":'$1/ 80',
		"([0-9][\\s]*)ενενηκοστ[όοάα]":'$1/ 90',

		"([0-9][\\s]*)εκατοστ[όοάα]":'$1/ 100',


		"(?:αντιπαραγοντικ[οό]\\s*(?:του\\s*)?([0-9]+))|(?:([0-9]+)\\s*αντιπαραγοντικ[οό])":'!$1$2',

		"(?:παραγοντικ[οό]\\s*(?:του\\s*)?([0-9]+))|(?:([0-9]+)\\s*παραγοντικ[οό])":'$1$2!',

		"(τις\\s+100)":'/100',

		"([0-9])([\\s]+)κ[οό]μμα([\\s]+)([0-9])":'$1.$4',
		"((?:-)?[0-9][\\s]*)κλε[ιί]νει(?:\\s+η)?\\s+παρ[εέ]νθεση":'$1)',
		"(ανο[ιί]γει(?:\\s+))?παρ[εέ]νθεση([\\s]*(-)?[0-9])":'($2',
		"μικρ[οό]τερο((?:\\s+)ή)?(?:\\s+)[ιί]σο(ν)?((?:\\s+)(απ[οό]|του))?":'≤',
		"μικρ[οό]τερο((?:\\s+)(απ[οό]|του))?":'<',
		"μεγαλ[υύ]τερο((?:\\s+)ή)?(?:\\s+)[ιί]σο(ν)?((?:\\s+)(απ[οό]|του))?":'≥',
		"μεγαλ[υύ]τερο((?:\\s+)(απ[οό]|του))?":'>',
		"δι[αά]φορο((?:\\s+)[ιί]σο(ν)?)?((?:\\s+)(με|του|απ[οό]))?":'≠',
		"([ιί]σο(ν)?((?:\\s+)με)?)|(μας(?:\\s+))?κ[αά]νει":'=',
		"[αά]πειρο":'∞',

		"υπ[οό]λοιπο\\s+του\\s+([0-9]+(?:[.][0-9]+)?)\\s+δι[αά](?:\\s+του)?\\s+([0-9]+(?:[.][0-9]+)?)":'$1%$2',
		"ακ[εέ]ραιη\\s+δια[ιί]ρεση\\s+του\\s+([0-9]+(?:[.][0-9]+)?)\\s+με\\s+το\\s+([0-9]+(?:[.][0-9]+)?)":'$1\\$2',
		"δια[ιί]ρεση\\s+του\\s+([0-9]+(?:[.][0-9]+)?)\\s+με\\s+το\\s+([0-9]+(?:[.][0-9]+)?)":'$1/$2',

		"πολ[λ]?[αά]πλασιασμ[οό][σς]\\s+του\\s+([0-9]+(?:[.][0-9]+)?)\\s+με\\s+το\\s+([0-9]+(?:[.][0-9]+)?)":'$1*$2',
		"πολ[λ]?[αά]πλασ[ιί]ασε\\s+το\\s+([0-9]+(?:[.][0-9]+)?)\\s+με\\s+το\\s+([0-9]+(?:[.][0-9]+)?)":'$1*$2',

		"με[ίι]ον|πλ[ηή]ν":'-',
		"και|συν":'+',
		"επ[ιί]|φορ[εέ]ς((?:\\s+)το)?":'*',
		"δι[αά](?:\\s+του)?":'/',
		"δ[υύ]ναμη|εις\\s+την":'^',

		"τετραγ[οω]νικ[ηή]\\s+ρ[ιί]ζα\\s+του\\s+([0-9]+(?:[.][0-9]+)?)":'$1//2',
		"κυβικ[ηή]\\s+ρ[ιί]ζα\\s+του\\s+([0-9]+(?:[.][0-9]+)?)":'$1//3',
 		"([0-9]+(?:[.][0-9]+)?)\\s+ρ[ιί]ζα\\s+του\\s+([0-9]+(?:[.][0-9]+)?)":'$2//$1',

		"([0-9]+(?:[.][0-9]+)?)\\s+ρ[ιί]ζα?\\s+([0-9]+(?:[.][0-9]+)?)":'$1//$2'
		// "ρ[ιί]ζα((?:\\s+)του)?":'√'

	},

	"en":{
		"zero":'0',"one":'1',"two":'2',"three":'3',"four":'4',"five":'5',"six":'6',"seven":'7',"eight":'8',"nine":'9',"ten":'10',"eleven":'11',"twelve":'12',
		"([3-9])teen":'1$1',"twenty([1-9])":'2$1',"thirty([1-9])":'3$1',"forty([1-9])":'4$1',"fifty([1-9])":'5$1',"6ty([1-9])":'6$1',"7ty([1-9])":'7$1',"8y([1-9])":'8$1',"9ty([1-9])":'9$1',
		"twenty":'20',"thirty":'30',"forty":'40',"fifty":'50',"6ty":'60',"7ty":'70',"8y":'80',"9ty":'90',

		"([1-9])(\\s*)hundred(\\s*)(and\\s*)?([0-9]{2})":'$1$5',"([1-9])(\\s*)hundred(\\s*)(and\\s*)?([0-9]{1})":'$10$5',"([1-9])(\\s*)hundred":'$100',
		"(a\\s*)hundred(\\s*)(and\\s*)?([0-9]{2})":'1$4',"(a\\s*)hundred(\\s*)(and\\s*)?([0-9]{1})":'10$4',"(a\\s*)hundred":'100',

		"([0-9]{1,3})(\\s*)thousand(\\s*)(and\\s*)?([0-9]{3})":'$1$5',
		"([0-9]{1,3})(\\s*)thousand(\\s*)(and\\s*)?([0-9]{2})":'$10$5',
		"([0-9]{1,3})(\\s*)thousand(\\s*)(and\\s*)?([0-9]{1})":'$100$5',
		"([0-9]{1,3})(\\s*)thousand":'$1000',

		"(a\\s*)?thousand(\\s*)(and\\s*)?([0-9]{3})":'1$4',
		"(a\\s*)?thousand(\\s*)(and\\s*)?([0-9]{2})":'10$4',
		"(a\\s*)?thousand(\\s*)(and\\s*)?([0-9]{1})":'100$4',
		"(a\\s*)?thousand":'1000',

		//"\\s*factorial":'!',
		//"minus((?:\\s+)[0-9])":'-$1',
		//"([0-9](?:\\s+))?plus((?:\\s+)[0-9])":'$1+$2',
		//"([0-9](?:\\s+))times((?:\\s+)[0-9])":'$1*$2',
		// "([0-9](?:\\s+))divide(?:\\s+)by((?:\\s+)[0-9])":'$1/$2',
		// "([0-9](?:\\s+))power((?:\\s+)[0-9])":'$1^$2',
		// "([0-9])((?:\\s+))point((?:\\s+))([0-9])":'$1.$4',
		//"[0-9](\\s*(?:is\\s+)?equals?(?:\\s+to)?\\s*)[0-9]":'$1=$2',

		"\\s*antifactorial\\s*[(]\\s*([0-9]+(?:[.][0-9]+)?)\\s*[)]":'!$1',
		"([0-9]+(?:[.][0-9]+)?)\\s+antifactorial":'!$1',

		"\\s*factorial\\s*[(]\\s*([0-9]+(?:[.][0-9]+)?)\\s*[)]":'$1!',
		"([0-9]+(?:[.][0-9]+)?)\\s+factorial":'$1!',

		"minus":'-',
		"(?<![a-z])(?:plus|and)(?!=[a-z])":'+',
		"times|multiply(?:\\s+by)?":'*',
		"divide\\s+by":'/',
		"(?:(?:raised?\\s+)?in(?:\\s+the)?\\s+)?power(?:\\s+of)?":'^',
		"([0-9])((?:\\s+))point((?:\\s+))([0-9])":'$1.$4',
		"(?:is\\s+)?equals?(?:\\s+to)?":'=',

		"root(?:\\s+of)?":'√',

		"close\\s+parenthesis":')',
		"(?:(open|set)\\s+)?parenthesis":'(',

	},

	"de":{
		"null":'0',"ein[se]?":'1',"zwei":'2',"drei":'3',"vier":'4',"f(?:[üu]|ue)nf":'5',"sechs":'6',"sieben":'7',"acht":'8',"neun":'9',
		"([1-9])undzwanzig":'2$1',"([1-9])und3(?:[ßs]|ss)ig":'3$1',"([1-9])und4zig":'4$1',"([1-9])und5zig":'5$1',"([1-9])undsechzig":'6$1',"([1-9])undsiebzig":'7$1',"([1-9])und8zig":'8$1',"([1-9])und9zig":'9$1',
		"([1-9])(\\s*)hundert(\\s*und)?(\\s*(zehn|elf|zw(?:[öo]|oe)lf|zwanzig|drei(?:[ßs]|ss)ig|vierzig|f(?:[üu]|ue)nfzig|sechzig|siebzig|achtzig|neunzig))":'$1$5',"([1-9])(\\s*)hundert(\\s*und)?(\\s*([1-9])[^0-9])":'$10$5',"([1-9])(\\s*)hundert(\\s*und)?(\\s*([1-9]{2}))":'$1$5',"([1-9])(\\s*)hundert":'$100',
		"([3-9])zehn":'1$1',"zehn":'10',"elf":'11',"zw(?:[öo]|oe)lf":'12',
		"zwanzig":'20',"3(?:[ßs]|ss)ig":'30',"4zig":'40',"5zig":'50',"sechzig":'60',"siebzig":'70',"8zig":'80',"9zig":'90',
		"([0-9]{1,3})(\\s*)tausend(\\s*)(und)?(\\s*)([0-9]{3})":'$1$6',"([0-9]{1,3})(\\s*)tausend(\\s*)(und)?(\\s*)([0-9]{2})":'$10$6',"([0-9]{1,3})(\\s*)tausend(\\s*)(und)?(\\s*)([0-9])":'$100$6',"([0-9]{1,3})(\\s*)tausend":'$1000',
		"tausend(\\s*)(und)?(\\s*)([0-9]{3})":'1$4',"tausend(\\s*)(und)?(\\s*)([0-9]{2})":'10$4',"tausend(\\s*)(und)?(\\s*)([0-9])":'100$4',"tausend":'1000',

		"([0-9]{1,3})(\\s*)million(\\s*)(und)?(\\s*)([0-9]{6})":'$1$6',
		"([0-9]{1,3})(\\s*)million(\\s*)(und)?(\\s*)([0-9]{5})":'$10$6',
		"([0-9]{1,3})(\\s*)million(\\s*)(und)?(\\s*)([0-9]{4})":'$100$6',
		"([0-9]{1,3})(\\s*)million(\\s*)(und)?(\\s*)([0-9]{3})":'$1000$6',
		"([0-9]{1,3})(\\s*)million(\\s*)(und)?(\\s*)([0-9]{2})":'$10000$6',
		"([0-9]{1,3})(\\s*)million(\\s*)(und)?(\\s*)([0-9]{1})":'$100000$6',
		"([0-9]{1,3})(\\s*)million":'$1000000',

		"million(\\s*)(und)?(\\s*)([0-9]{6})":'1$4',
		"million(\\s*)(und)?(\\s*)([0-9]{5})":'10$4',
		"million(\\s*)(und)?(\\s*)([0-9]{4})":'100$4',
		"million(\\s*)(und)?(\\s*)([0-9]{3})":'1000$4',
		"million(\\s*)(und)?(\\s*)([0-9]{2})":'10000$4',
		"million(\\s*)(und)?(\\s*)([0-9]{1})":'100000$4',
		"million":'1000000',


		"([0-9]{1,3})(\\s*)milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{9})":'$1$6',
		"([0-9]{1,3})(\\s*)milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{8})":'$10$6',
		"([0-9]{1,3})(\\s*)milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{7})":'$100$6',
		"([0-9]{1,3})(\\s*)milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{6})":'$1000$6',
		"([0-9]{1,3})(\\s*)milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{5})":'$10000$6',
		"([0-9]{1,3})(\\s*)milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{4})":'$100000$6',
		"([0-9]{1,3})(\\s*)milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{3})":'$1000000$6',
		"([0-9]{1,3})(\\s*)milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{2})":'$10000000$6',
		"([0-9]{1,3})(\\s*)milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{1})":'$100000000$6',
		"([0-9]{1,3})(\\s*)milliarde[n]?":'$1000000000',

		"milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{9})":'1$4',
		"milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{8})":'10$4',
		"milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{7})":'100$4',
		"milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{6})":'1000$4',
		"milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{5})":'10000$4',
		"milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{4})":'100000$4',
		"milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{3})":'1000000$4',
		"milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{2})":'10000000$4',
		"milliarde[n]?(\\s*)(und)?(\\s*)([0-9]{1})":'100000000$4',
		"milliarde[n]?":'1000000000',

		"([0-9]{1,3})(\\s*)billion(\\s*)(und)?(\\s*)([0-9]{12})":'$1$6',
		"([0-9]{1,3})(\\s*)billion(\\s*)(und)?(\\s*)([0-9]{11})":'$10$6',
		"([0-9]{1,3})(\\s*)billion(\\s*)(und)?(\\s*)([0-9]{10})":'$100$6',
		"([0-9]{1,3})(\\s*)billion(\\s*)(und)?(\\s*)([0-9]{9})":'$1000$6',
		"([0-9]{1,3})(\\s*)billion(\\s*)(und)?(\\s*)([0-9]{8})":'$10000$6',
		"([0-9]{1,3})(\\s*)billion(\\s*)(und)?(\\s*)([0-9]{7})":'$100000$6',
		"([0-9]{1,3})(\\s*)billion(\\s*)(und)?(\\s*)([0-9]{6})":'$1000000$6',
		"([0-9]{1,3})(\\s*)billion(\\s*)(und)?(\\s*)([0-9]{5})":'$10000000$6',
		"([0-9]{1,3})(\\s*)billion(\\s*)(und)?(\\s*)([0-9]{4})":'$100000000$6',
		"([0-9]{1,3})(\\s*)billion(\\s*)(und)?(\\s*)([0-9]{3})":'$1000000000$6',
		"([0-9]{1,3})(\\s*)billion(\\s*)(und)?(\\s*)([0-9]{2})":'$10000000000$6',
		"([0-9]{1,3})(\\s*)billion(\\s*)(und)?(\\s*)([0-9]{1})":'$100000000000$6',
		"([0-9]{1,3})(\\s*)billion":'$1000000000000',

		"billion(\\s*)(und)?(\\s*)([0-9]{12})":'1$4',
		"billion(\\s*)(und)?(\\s*)([0-9]{11})":'10$4',
		"billion(\\s*)(und)?(\\s*)([0-9]{10})":'100$4',
		"billion(\\s*)(und)?(\\s*)([0-9]{9})":'1000$4',
		"billion(\\s*)(und)?(\\s*)([0-9]{8})":'10000$4',
		"billion(\\s*)(und)?(\\s*)([0-9]{7})":'100000$4',
		"billion(\\s*)(und)?(\\s*)([0-9]{6})":'1000000$4',
		"billion(\\s*)(und)?(\\s*)([0-9]{5})":'10000000$4',
		"billion(\\s*)(und)?(\\s*)([0-9]{4})":'100000000$4',
		"billion(\\s*)(und)?(\\s*)([0-9]{3})":'1000000000$4',
		"billion(\\s*)(und)?(\\s*)([0-9]{2})":'10000000000$4',
		"billion(\\s*)(und)?(\\s*)([0-9]{1})":'100000000000$4',
		"billion":'1000000000000',

		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{15})":'$1$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{14})":'$10$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{13})":'$100$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{12})":'$1000$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{11})":'$10000$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{10})":'$100000$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{9})":'$1000000$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{8})":'$10000000$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{7})":'$100000000$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{6})":'$1000000000$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{5})":'$10000000000$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{4})":'$100000000000$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{3})":'$1000000000000$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{2})":'$10000000000000$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{1})":'$100000000000000$6',
		"([0-9]{1,3})(\\s*)billiarde[n]?":'$1000000000000000',

		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{15})":'1$4',
		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{14})":'10$4',
		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{13})":'100$4',
		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{12})":'1000$4',
		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{11})":'10000$4',
		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{10})":'100000$4',
		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{9})":'1000000$4',
		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{8})":'10000000$4',
		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{7})":'100000000$4',
		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{6})":'1000000000$4',
		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{5})":'10000000000$4',
		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{4})":'100000000000$4',
		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{3})":'1000000000000$4',
		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{2})":'10000000000000$4',
		"billiarde[n]?(\\s*)(und)?(\\s*)([0-9]{1})":'100000000000000$4',
		"billiarde[n]?":'1000000000000000',

		"([0-9])\\s*Fakultät":'$1!',

		"minus((?:\\s+)[0-9])":'-$1',
		"([0-9](?:\\s+))?(plus|und)((?:\\s+)[0-9])":'$1+$3',
		"([0-9](?:\\s+))mal((?:\\s+)[0-9])":'$1*$2',
		"([0-9](?:\\s+))(geteilt(?:\\s+))?durch((?:\\s+)[0-9])":'$1/$3',
		"([0-9](?:\\s+))hoch((?:\\s+)[0-9])":'$1^$2',
		"([0-9])((?:\\s+))komma((?:\\s+))([0-9])":'$1.$4',
		"weniger(?:\\s+)gleich((?:\\s+)zu)?":'≤',
		"weniger((?:\\s+)als)?":'<',
		"gr(?:[öo]|oe)(?:[ßs]|ss)erer(?:\\s+)gleich((?:\\s+)zu)?":'≥',
		"gr(?:[öo]|oe)(?:[ßs]|ss)er((?:\\s+)als)?":'>',
		"(geschlossenen(?:\\s+)klammer)|(klammer(?:\\s+)zu)":')',
		"(offene(?:\\s+))?klammer((?:\\s+)auf)?":'(',
		"unendlichkeit":'∞',
		"ist gleich":'=',
		"quadrat(?:\\s+)wurzel(\\s+aus)?":'2 √',
		"dritte(?:\\s+)wurzel(\\s+aus)?":'3 √',
		"wurzel((?:\\s+)aus)?":'√'
	}
};

/**
 * Property for setting the library's version number.
 * @type {Array}
 */
lvzmath.version = [2,2,48];

// If called using nodejs
if ( typeof exports == 'object' ) {

	exports.lvzmath = lvzmath;

}
